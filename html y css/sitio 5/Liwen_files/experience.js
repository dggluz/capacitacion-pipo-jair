$(document).ready(function() {
	'use strict';

	$('nav ul li a').not('#catalogo').click(function() {
		var className = $(this).attr('href').split('#')[1];
		$('nav ul li a').removeClass('selected');
		if($('main').hasClass(className) && !$('main').hasClass('hidden')) {
			$('main')
				.addClass('hidden');
		}
		else {
			$('main')
				.removeClass($('main').get(0).className)
				.addClass(className);
			$(this).addClass('selected');
		}
	}).each(function() {
		if(document.location.hash.substr(1) == $(this).attr('href').split('#')[1]) {
			$(this).click();
		}
	});

	$('#contacto form').submit(function(e) {
		e.preventDefault();

		var $nombre = $(this).find('#nombre'),
			$mail = $(this).find('#mail'),
			$comentarios = $(this).find('#comentarios'),
			$fields = $nombre.add($mail).add($comentarios).add($(this).find('[type=submit]')).attr('disabled', true);
		if(!$nombre.val().trim()) {
			alert('Debes completar el nombre.');
			$fields.attr('disabled', false);
			$nombre.focus();
			return false;
		}
		if(!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($mail.val().trim())) {
			alert('El mail es inválido.');
			$fields.attr('disabled', false);
			$mail.focus();
			return false;
		}
		if(!$comentarios.val().trim()) {
			alert('Debes escribir algún comentario.');
			$fields.attr('disabled', false);
			$comentarios.focus();
			return false;
		}

		$.ajax({
			url: 'contacto.php',
			type: 'POST',
			data: {
				nombre: $nombre.val(),
				mail: $mail.val(),
				comentarios: $comentarios.val()
			},
			success: function() {
				// TODO: cambiar esto.
				alert('Tu mensaje ha sido enviado');
				$fields.attr('disabled', false);
				$nombre.add($mail).add($comentarios).val('');
			},
			error: function() {
				alert('Ocurrió un error. No se pudo enviar el mensaje.');
				$fields.attr('disabled', false);
			}
		});
	});
});