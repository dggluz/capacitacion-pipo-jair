Ejercicios
=======
**IMPORTANTE**: en los ejercicios de funcional **NO SE PUEDEN** usar las estructuras de control de flujo *while*, *for*, *switch*, etc. Sólo pueden usarse *if* y *else* si el ejercicio lo especifica explícitamente. Siempre puede usarse el *operador ternario*. **TAMPOCO PUEDEN HACERSE ASIGNACIONES DESTRUCTIVAS**, salvo que se trate de definir funciones en el scope global.

**Nota**: cuando se escriba: "*nombreFuncion/numero*" el número indica la cantidad de parámetros de la función ("*aridad*").
##Básicos
- *a.* Definir la función **esMultiploDeTres/1**, que devuelve *true* si un número es múltiplo de 3, por ejemplo: 
```javascript
esMultiploDeTres(9); // Devuelve true
```
- *b.* Definir la función **esMultiploDe/2**, que recibe dos números y devuelve *true* si el primero es múltiplo del segundo, por ejemplo: 
```javascript
esMultiploDe(9, 2); // Devuelve false
```
- *c.* Reescribir *esMultiploDeTres/1* pero apoyándose en *esMultiploDe/2*.
- *d.* Definir la función **cubo/1**, devuelve el cubo de un número. 
- *e.* Definir la función **esBisiesto/1**, que indica si un año es bisiesto. (Un año es bisiesto si es divisible por 400 o bien si es divisible por 4 pero no es divisible por 100).

**Nota**: Resolverlo reutilizando la función *esMultiploDe/2*

- *f.* Definir la función **cantidadDias/1** que recibe un año (por ejemplo: *2014*) y devuelve la cantidad de días que tiene.
- *g.* Definir la función **abs/1** que recibe un número y devuelve su valor absoluto:
```javascript
abs(-4.8); // Devuelve 4.8
```
- *h.* Definir la función **minimo/2** que recibe dos números y devuelve el mínimo entre ambos.
- *i.* Definir la función **maximo/2** que recibe dos números y devuelve el máximo entre ambos.
- *j.* Definir la función **minimoDeTres/3** que recibe **tres** números y devuelve el mínimo de ellos.

**Nota**: reutilizar la función *minimo/2*.

- *k.* Definir la función **probabilidadDeAprobar/1** que reciba una característica de un alumno (un *String*) y devuelva la probabilidad que tiene de aprobar, de acuerdo a la siguiente tabla:

| Característica         | Probabilidad de aprobar |
|------------------------|-------------------------|
| Vago                   | 10%                     |
| Inteligente            | 60%                     |
| Estudioso              | 100%                    |
| En cualquier otro caso | 50%                     |

Por ejemplo:
```javascript
probabilidadDeAprobar('vago'); // Devuelve 10
probabilidadDeAprobar('se duerme en las clases'); // Devuelve 50
```
**Nota**: sólo en este ejercicio se pueden usar **if** y **else**.
##Ejercicio básico de integración: *Pinos*
En una plantación de pinos, de cada árbol se conoce la altura expresada en *cm*. El peso de un pino se puede calcular a partir de la altura así: **3 kg x cm** *hasta 3 metros*, **2 kg x cm** *arriba de los 3 metros*. Por ejemplo: *2 metros* -> *600 kg*, *5 metros* -> *1000 kg*.
Los pinos se usan para llevarlos a una fábrica de muebles, a la que *le sirven árboles de entre 400 y 1000 kilos*, un pino fuera de este rango no le sirve a la fábrica.
Para esta situación:

- *a.* Definir la función **pesoPino/1**, recibe la altura de un pino y devuelve su peso.
- *b.* Definir la función **esPesoUtil/1**, recibe un peso en *kg* y devuelve *true* si un pino de ese peso le sirve a la fábrica, y *false* en caso contrario.
- *c.* Definir la función **sirvePino/1**, recibe la altura de un pino y devuelve *true* si un pino de esa altura le sirve a la fábrica, y *false* en caso contrario.
- *d.* Definir la función **prioridadPino/1**, que recibe la altura de un pino y en base a ella clasifica al pino:
	- Si la altura es exactamente igual a 2 metros, entonces el pino tiene prioridad *alta*.
	- Sino si la altura no supera los 3 metros, su peso es útil y además es mayor a 800 *kg*, tiene prioridad *media*.
	- Si el pino no le sirve a la empresa, entonces tiene prioridad *baja*.
	- En cualquier otro caso, se considera prioridad obsoleto y no se hará nada con él.

**Nota**: únicamente pare el punto *d*, pueden usarse **if** y **else**.
