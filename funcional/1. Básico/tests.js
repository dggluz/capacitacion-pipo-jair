QUnit.test('Básicos', function(assert) {
	// a y c
	assert.ok(esMultiploDeTres(9), '9 es múltiplo de 3');
	assert.ok(!esMultiploDeTres(8), '8 NO es múltiplo de 3');

	// b
	assert.ok(esMultiploDe(8, 2), '8 es múltiplo de 2');
	assert.ok(!esMultiploDe(15, 2), '15 NO es múltiplo de 2');

	// d
	assert.equal(cubo(3), 27, 'El cubo de 3 es 27');

	// e
	assert.ok(!esBisiesto(1900), '1900 no fue bisiesto (múltiplo de 100 pero no de 400)');
	assert.ok(esBisiesto(2000), '2000 sí fue bisiesto (múltiplo de 400)');
	assert.ok(!esBisiesto(1997), '1997 no fue bisiesto (no es múltiplo de 4)');
	assert.ok(esBisiesto(2004), '2004 sí fue bisiesto (es múltiplo de 4)');

	// f
	assert.equal(cantidadDias(2000), 366, '2000 fue bisiesto, por eso tiene 366 días');
	assert.equal(cantidadDias(1997), 365, '1997 no fue bisiesto, así que tiene 365 días');

	// g
	assert.equal(abs(-4.8), 4.8, 'abs(-n) = n, si n < 0');
	assert.equal(abs(4.8), 4.8, 'abs(n) = n, si n >= 0');

	// h
	assert.equal(minimo(-4, 3), -4, 'El mínimo entre -4 y 3 es -4');

	// i
	assert.equal(maximo(-4, 3), 3, 'El máximo entre -4 y 3 es 3');

	// j
	assert.equal(minimoDeTres(-4, -7, 3), -7, 'El mínimo entre -4, -7 y 3 es -7');

	// k
	assert.equal(probabilidadDeAprobar('vago'), 10, 'Alguien vago difícilmente apruebe');
	assert.equal(probabilidadDeAprobar('se duerme en las clases'), 50, 'Si se duerme en las clases quizás apruebe');
	assert.equal(probabilidadDeAprobar('inteligente'), 60, 'Si es inteligente es probable que apruebe');
	assert.equal(probabilidadDeAprobar('estudioso'), 100, 'Alguien estudioso seguro aprueba');
});

QUnit.test('Integración de los ejercicios básicos: pinos', function(assert) {
	// a
	assert.equal(pesoPino(200), 600, 'Un pino de 2 m pesa 600 kg');
	assert.equal(pesoPino(500), 1300, 'Un pino de 5 m pesa 1300 kg');

	// b
	assert.ok(!esPesoUtil(200), 'Un pino de 200 kg no le sirve a la fábrica');
	assert.ok(esPesoUtil(401), 'Un pino de 401 kg sí le sirve a la fábrica');
	assert.ok(!esPesoUtil(1200), 'Un pino de 1200 kg no le sirve a la fábrica');
	
	// c
	assert.ok(!sirvePino(50), 'Un pino de medio metro no le sirve a la fábrica');
	assert.ok(sirvePino(200), 'Un pino de 2 metros le sirve a la fábrica');
	assert.ok(!sirvePino(550), 'Un pino de 5 metros y medio no le sirve a la fábrica');

	// d
	assert.equal(prioridadPino(200), 'alta', 'Un pino de 2 m tiene prioridad alta');
	assert.equal(prioridadPino(290), 'media', 'Un pino de 2.9 m tiene prioridad media');
	assert.equal(prioridadPino(50), 'baja', 'Un pino de medio metro tiene prioridad baja');
	assert.equal(prioridadPino(301), 'obsoleta', 'Un pino de 3.01 m tiene prioridad obsoleta');
	
});