Ejercicios
=====
##Movimientos
Teniendo *estructuras* de la forma **{x: 2,y: 3}** que representan puntos en el plano:

- *a.* Definir las funciones que indican desplazamiento en la dirección de los puntos cardinales, en una unidad, devolviendo la nueva ubicación del punto:
```javascript
norte({x: 3, y: 9}); // Devuelve {x: 4, y: 9}
este({x: 3, y: 9}); // Devuelve {x: 3, y: 10}
```
- *b.* Utilizando las anteriores, definir las funciones correspondientes a las direcciones intermedias (noreste, noroeste, sudeste, sudoeste):
```javascript
noreste({x: 4, y: 9}); // Devuelve {x: 5, y: 10}
```
- *c.* Dados dos puntos, devolver la distancia que los separa.
```javascript
distancia({x: 2, y: 4}, {x: 5, y: 8}); // Devuelve 5
```
##Notas
Representamos las notas que se sacó un alumno en dos parciales mediante una *estructura* **{nota1: 3, nota2: 8}**. Por ejemplo: un patito en el 1ro y un 7 en el 2do se representan mediante la *estructura* **{nota1: 2, nota2: 7}**.

A partir de esto:

- *a.* Definir la función **esNotaBochazo/1**, que recibe un número y devuelve *true* si no llega a **4**, *false* en caso contrario. No vale usar **if**, **switch**, ni el **operador ternario**.
- *b.* Definir la función **aprobo/1**, que recibe una *estructura* de notas e indica si una persona que se sacó esas notas aprueba. Usar *esNotaBochazo/1*.
- *c.* Definir la función **promociono/1**, que indica si promocionó, para eso las dos notas tienen que sumar al menos 14 y además debe haberse sacado al menos 6 en los dos parciales.

---------------------------------------

Tenemos ahora dos parciales con dos recuperatorios, lo representamos mediante una **estructura anidada**:
```javascript
{
	parciales: {
		nota1: 3, 
		nota2: 2
	}, 
	recuperatorios: {
		nota1: 6,
		nota2: 8
	}
}
```

Si una persona no rindió un recuperatorio, entonces ponemos un "*-1*" en el lugar correspondiente. Observamos que con la codificación elegida, *siempre la mejor nota es el máximo entre nota del parcial y nota del recuperatorio*. Considerar que vale recuperar para promocionar.

- *d.* Definir la función **notasFinales/1** que recibe una *estructura* de notas con recuperatorios y devuelve la *estructura* de notas que corresponde a las notas finales del alumno para el 1er y el 2do parcial. Por ejemplo:
```javascript
notasFinales({
	parciales: {
		nota1: 2,
		nota2: 7
	},
	recuperatorios: {
		nota1: 6,
		nota2: -1
	}
}); // Devuelve {nota1: 6, nota2: 7}
notasFinales({
	parciales: {
		nota1: 2,
		nota2: 2
	},
	recuperatorios: {
		nota1: 6,
		nota2: 2
	}
}); // Devuelve {nota1: 6, nota2: 2}
notasFinales({
	parciales: {
		nota1: 8,
		nota2: 7
	},
	recuperatorios: {
		nota1: -1,
		nota2: -1
	}
}); // Devuelve {nota1: 8, nota2: 7}
```
- *e.* Definir la función **recuperoDeGusto/1** que dada la *estructura* que representa a un alumno, devuelve *true* si el alumno, pudiendo promocionar con los parciales (o sea sin recuperatorios), igual rindió al menos un recuperatorio. Vale definir funciones auxiliares.
