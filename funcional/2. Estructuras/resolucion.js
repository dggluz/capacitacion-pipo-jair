var norte = function(punto){
	return {
		x: punto.x,
		y: punto.y + 1,
};
};
var sur = function(punto){
	return {
		x: punto.x,
		y: punto.y - 1,
	};
};

var este = function(punto){
	return {
		x: punto.x + 1,
		y: punto.y,
	};
};

var oeste = function(punto){
	return {
		x: punto.x - 1,
		y: punto.y,
	};
};

var noreste = function(punto){
	return norte(este(punto));
};

var nortenorte = function(punto){
	return norte(norte(punto));
};

var noroeste = function(punto){
	return norte(oeste(punto));
};

var sudeste = function(punto){
	return sur(este(punto));
};

var sudoeste = function(punto) {
	return sur(oeste(punto));
};

var raiz = Math.sqrt;
var cuadrado = function(numero){
	return numero*numero;
};

var distancia = function(punto1,punto2){
	return raiz(cuadrado(punto1.x - punto2.x)+ cuadrado(punto1.y - punto2.y));
};
