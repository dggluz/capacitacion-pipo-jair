QUnit.test('Movimientos', function(assert) {
	// a
	assert.deepEqual(norte({x: 3, y: 9}), {x: 3, y: 10}, 'Ir al norte aumenta una unidad la coordenada y');
	assert.deepEqual(sur({x: 3, y: 9}), {x: 3, y: 8}, 'Ir al sur disminuye una unidad la coordenada y');
	assert.deepEqual(este({x: 3, y: 9}), {x: 4, y: 9}, 'Ir al este aumenta una unidad la coordenada x');
	assert.deepEqual(oeste({x: 3, y: 9}), {x: 2, y: 9}, 'Ir al oeste disminuye una unidad la coordenada x');

	// b
	assert.deepEqual(noreste({x: 3, y: 9}), {x: 4, y: 10}, 'Ir al noreste aumenta una unidad la coordenada x, y otra la y');
	assert.deepEqual(noroeste({x: 3, y: 9}), {x: 2, y: 10}, 'Ir al noroeste disminuye una unidad la coordenada x, pero aumenta la y');
	assert.deepEqual(sudeste({x: 3, y: 9}), {x: 4, y: 8}, 'Ir al sudeste aumenta una unidad la coordenada x, pero disminuye la y');
	assert.deepEqual(sudoeste({x: 3, y: 9}), {x: 2, y: 8}, 'Ir al sudoeste disminuye una unidad la coordenada x, y otra la y');

	// c
	assert.equal(distancia({x: 2, y: 4}, {x: 5, y: 8}), 5, 'La distancia entre (2; 4) y (5; 8) es de 5');
});

QUnit.test('Notas', function(assert) {
	// a
	assert.ok(esNotaBochazo(3), 'Un 3 es un bochazo');
	assert.ok(!esNotaBochazo(8), 'Un 8 NO es un bochazo');

	// b
	assert.ok(!abrobo({nota1: 3, nota2: 1}), 'No aprobó sin ningún parcial aprobado');
	assert.ok(!abrobo({nota1: 3, nota2: 8}), 'No aprobó sin el primer parcial aprobado');
	assert.ok(!abrobo({nota1: 10, nota2: 2}), 'No aprobó sin el segundo parcial aprobado');
	assert.ok(abrobo({nota1: 10, nota2: 8}), 'Aprobó los dos parciales, por lo tanto está aprobado');

	// c
	assert.ok(promociono({nota1: 10, nota2: 8}), 'Se sacó al menos 6 en ambos parciales y además suma al menos 14: promocionó');
	assert.ok(!promociono({nota1: 10, nota2: 4}), 'Suma al menos 14, pero no se sacó al menos 6 en ambos parciales: no promocionó');
	assert.ok(!promociono({nota1: 6, nota2: 6}), 'Se sacó al menos 6 en ambos parciales pero no suma al menos 14: no promocionó');

	// d
	assert.deepEqual(notasFinales({parciales: {nota1: 2, nota2: 7}, recuperatorios: {nota1: 6, nota2: -1}}), {nota1: 6, nota2: 7}, 'Elegimos las mejores notas para las notas finales (dio un recuperario).');
	assert.deepEqual(notasFinales({parciales: {nota1: 2, nota2: 2}, recuperatorios: {nota1: 6, nota2: 2}}), {nota1: 6, nota2: 2}, 'Elegimos las mejores notas para las notas finales (dio dos recuperatorios).');
	assert.deepEqual(notasFinales({parciales: {nota1: 8, nota2: 7}, recuperatorios: {nota1: -1, nota2: -1}}), {nota1: 8, nota2: 7}, 'Elegimos las mejores notas para las notas finales (no dio recuperatorios).');

	// e
	assert.ok(recuperoDeGusto({parciales: {nota1: 8, nota2: 7}, recuperatorios: {nota1: 6, nota2: -1}}), 'No necesitaba dar ni un recuperatorio porque promocionaba con los parciales: recuperó de gusto');
	assert.ok(!recuperoDeGusto({parciales: {nota1: 5, nota2: 7}, recuperatorios: {nota1: 6, nota2: -1}}), 'No le alcanzaba para promocionar con los parciales: NO recuperó de gusto');
	assert.ok(!recuperoDeGusto({parciales: {nota1: 8, nota2: 7}, recuperatorios: {nota1: -1, nota2: -1}}), 'No dio ningún recuperatorio: NO recuperó de gusto');
});