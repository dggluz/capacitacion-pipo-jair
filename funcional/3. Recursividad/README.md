Implementar las siguientes funciones:

- [*factorial/1*](https://es.wikipedia.org/wiki/Factorial)
- [*fibonacci/1*](https://es.wikipedia.org/wiki/Sucesi%C3%B3n_de_Fibonacci)
- *multiplicacion/2*
- *esPar/1* (sólo puede recibir números positivos)
- *esPar/1* (versión también para números negativos)
- *esDivisiblePor/2*
- *divisionEntera/2*
- *primo/1* -> *Boolean*
- *siguientePrimo/1*

