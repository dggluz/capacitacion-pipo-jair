QUnit.test('Recursividad', function(assert) {
	assert.equal(factorial(6), 720, 'El factorial de 6 es 720');
	assert.equal(factorial(10), 3628800, 'El factorial de 10 es 3628800');
	assert.equal(factorial(0), 1, 'El factorial de 0 es 1');

	assert.equal(multiplicacion(0, 546), 0, 'La multiplicación de 0 por cualquier número me da 0');
	assert.equal(multiplicacion(4, 3), 12, '4 * 3 = 12');
	assert.equal(multiplicacion(5, 6), 30, '5 * 6 = 30');

	assert.equal(esPar(5), false, '5 no es par');
	assert.equal(esPar(8), true, '8 es par');
	assert.equal(esPar(-5), false, '-5 no es par');
	assert.equal(esPar(-8), true, '-8 es par');

	assert.equal(fibonacci(8), 21, 'El fibonacci es 21');
	assert.equal(fibonacci(0), 0, 'El fibonacci de 0 es 0');

	assert.equal(esDivisiblePor(27, 3), true, '27 es divisible por 3');
	assert.equal(esDivisiblePor(28, 3), false, '28 NO es divisible por 3');
	assert.equal(esDivisiblePor(100, 4), true, '100 es divisible por 4');
	assert.equal(esDivisiblePor(101, 25), false, '101 NO es divisible por 25');

	assert.equal(divisionEntera(101, 25), 4, '101 / 25 = 4');
	assert.equal(divisionEntera(27, 3), 9, '27 / 3 = 9');
	assert.equal(divisionEntera(4, 4), 1, '4 / 4 = 1');
	assert.equal(divisionEntera(100, 1), 100, '100 / 1 = 100');

	assert.equal(primo(27), false, 'el 27 no es un número primo');
	assert.equal(primo(29), true, 'el 29 es primo');
	assert.equal(primo(17), true, 'el 17 es primo');

	assert.equal(siguientePrimo(11), 13, 'El siguiente número primo del 11 es el 13');
	assert.equal(siguientePrimo(13), 17, 'El siguiente número primo del 13 es el 17');
	assert.equal(siguientePrimo(17), 19, 'El siguiente número primo del 17 es el 19');
});
