var cabeza = function(lista) {
	return lista[0];
};
var cuello = function(lista) {
	return lista[1];
};

var cola = function(lista) {
	var ret = lista.map(function(a) {
		return a;
	});
	ret.shift();
	return ret;
};

var concatenar = function(lista1, lista2) {
	return lista1.concat(lista2);
};

var esVacia = function(lista) {
	return !lista.length;
};

var stringAListaDeLetras = function(string) {
	return string.split('');
};