//esVacia
//cabeza
//cola
//concatenar

var largo = function(lista) {
	return esVacia(lista) ? 0 : 1 + largo(cola(lista));
};

var pertenece = function(elemento, lista) {
	return !esVacia(lista) && (cabeza(lista) == elemento || pertenece(elemento, cola(lista)));
};

var esVocal = function(unaLetra) {
	return pertenece(unaLetra, ['a', 'e', 'i', 'o', 'u']);
};

var cuantasVocales = function(listaDeLetras) {
	return esVacia(listaDeLetras) ? 0 : (esVocal(cabeza(listaDeLetras)) ? 1 : 0) + cuantasVocales(cola(listaDeLetras));
};

var vocales = function(listaDeLetras) {
	return esVacia(listaDeLetras) ? [] :
		(esVocal(cabeza(listaDeLetras)) ? concatenar([cabeza(listaDeLetras)], vocales(cola(listaDeLetras))) : vocales(cola(listaDeLetras)));
};

var menoresA = function(listaDeNumeros, numero) {
	return esVacia(listaDeNumeros) ? [] : 
		(cabeza(listaDeNumeros) < numero ? concatenar([cabeza(listaDeNumeros)], menoresA(cola(listaDeNumeros), numero)) : menoresA(cola(listaDeNumeros), numero));
};

var sumatoria = function(listaDeNumeros){
	return esVacia(listaDeNumeros) ? 0 : cabeza(listaDeNumeros) + sumatoria(cola(listaDeNumeros));
};

var productoria = function(listaDeNumeros){
	return esVacia(listaDeNumeros) ? 1 : cabeza(listaDeNumeros) * productoria(cola(listaDeNumeros));
};

var promedio = function(listaDeNumeros){
	return sumatoria(listaDeNumeros) / largo(listaDeNumeros);
};

var promedios = function(listaDeListas) {
	return esVacia(listaDeListas) ? [] : concatenar([promedio(cabeza(listaDeListas))], promedios(cola(listaDeListas)));
};

var sinRepetidos = function(lista) {
	return esVacia(lista) ? [] : pertenece(cabeza(lista), cola(lista)) ? sinRepetidos(cola(lista)) : concatenar([cabeza(lista)], sinRepetidos(cola(lista)));
};

var interseccion = function(lista1, lista2) {
	return esVacia(lista1) ? [] : 
		pertenece(cabeza(lista1), lista2) ? concatenar([cabeza(lista1)], interseccion(cola(lista1), lista2)) : interseccion(cola(lista1), lista2);
};