var discotequers = [
	{
		nombre: 'fer',
		levante: 500,
		aguanteAlcohol: 10,
		tragosTomados: [
			{
				nombre: 'Coca cola',
				cantidad: 1
			},
			{
				nombre: 'Sprite Zero',
				cantidad: 1
			}
		]
	},
	{
		nombre: 'mati',
		levante: 1670,
		aguanteAlcohol: 2000,
		tragosTomados: [
			{
				nombre: 'Cerveza',
				cantidad: 2
			}
		]
	},
	{
		nombre: 'german',
		levante: 40000,
		aguanteAlcohol: 200000,
		tragosTomados: [
			{
				nombre: 'Grog XD',
				cantidad: 25
			},
			{
				nombre: 'Cerveza',
				cantidad: 1
			}
		]
	},
	{
		nombre: 'flor',
		levante: 5000,
		aguanteAlcohol: 15,
		tragosTomados: [
			{
				nombre: 'Grapa',
				cantidad: 1
			}
		]
	}
];

var bebidas = [
	{
		nombre: 'Coca cola',
		graduacionAlcoholica: 0
	},
	{
		nombre: 'Grog XD',
		graduacionAlcoholica: 350
	},
	{
		nombre: 'Sprite Zero',
		graduacionAlcoholica: 0
	},
	{
		nombre: 'Cerveza',
		graduacionAlcoholica: 10
	},
	{
		nombre: 'Grapa',
		graduacionAlcoholica: 40
	}
];

var nombre = function(persona) {
	return persona.nombre;
};

var levante = function(persona) {
	return persona.levante;
};

var aguante = function(persona) {
	return persona.aguanteAlcohol;
};

var tragos = function(persona) {
	return persona.tragosTomados;
};

var bebida = function(trago) {
	return trago.nombre;
};

var graduacion = function(trago) {
	return trago.graduacionAlcoholica;
};

// 1.a
var datosDe = function(personaBuscada) {
	return find(function(persona) {
		return nombre(persona) == personaBuscada;
	}, discotequers);
};

// 1.b
var graduacionAlcoholica = function(nombreTrago) {
	return graduacion(find(function(trago) {
		return bebida(trago) == nombreTrago;
	}, bebidas));
};

// 1.c
var alcoholEnSangreDe = function(nombre) {
	return sum(map(function(trago) {
		return graduacionAlcoholica(trago.nombre) * trago.cantidad;
	}, tragos(datosDe(nombre))));
};

// 2.a
var estaBorracho = function(nombre) {
	return aguante(datosDe(nombre)) < alcoholEnSangreDe(nombre);
};