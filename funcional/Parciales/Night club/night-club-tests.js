QUnit.test('Night club', function(assert) {
	// 1.a
	assert.deepEqual(datosDe('german'), {
		nombre: 'german',
		levante: 40000,
		aguanteAlcohol: 200000,
		tragosTomados: [
			{
				nombre: 'Grog XD',
				cantidad: 25
			},
			{
				nombre: 'Cerveza',
				cantidad: 1
			}
		]
	}, 'Obtener la informacion de Germán');

	// 1.b
	assert.equal(graduacionAlcoholica('Grog XD'), 350, 'Obtener la graduación alcohólica de Grog XD');
	// 1.c
	assert.equal(alcoholEnSangreDe('german'), 8760, 'Obtener el alcohol en sangre de Germán');
	// 2.a
	assert.equal(estaBorracho('german'), false, 'Averiguar si Germán está borracho');
});