
var head = function(lista) {
	return lista[0];
};
var tail = function(lista) {
	return lista.slice(1);
};
var empty = function(lista) {
	return !lista.length;
};
var concat = function(lista1, lista2) {
	return lista1.concat(lista2);
};
var length = function(lista) {
	return empty(lista) ? 0 : 1 + length(tail(lista));
};
var elem = function(elemento, lista) {
	return head(lista) == elemento || (!empty(lista) && elem(elemento, tail(lista)));
};
var find = function(condicion, lista) {
	return head(filter(condicion, lista));
};
var aplicarParcialmente = function(fn, parametro) {
	return function(otroParametro) {
		return fn(parametro, otroParametro);
	};
};
var flip = function(fn) {
	return function(a, b) {
		return fn(b, a);
	};
};
var map = function(transformacion, lista) {
	return empty(lista) ? lista : 
		concat([transformacion(head(lista))], map(transformacion, tail(lista)));
};
var filter = function(filtro, lista) {
	return empty(lista) ? lista : 
		filtro(head(lista)) ? 	
			concat([head(lista)], filter(filtro, tail(lista))) 
			: filter(filtro, tail(lista));
};
var all = function(condicion, lista) {
	return length(filter(condicion, lista)) == length(lista);
};
var any = function(condicion, lista) {
	return length(filter(condicion, lista)) > 0;
};
var sum = function(lista) {
	return empty(lista) ? 0 : head(lista) + sum(tail(lista));
};